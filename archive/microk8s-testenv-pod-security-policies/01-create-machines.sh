#!/bin/bash 

echo "Unremember IP's from machines from last run"
echo > .ip_list

# echo "Show training project and its id: doctl projects list | grep -i training"
doctl projects list | grep -i training
# Project training from digitalocean account 
PROJECT_ID=$(doctl projects list | grep -i training | cut -d' ' -f1)

# Functions 
function createMachine {

  MACHINE_ID=$1
  SIZE=$2
  USER_DATA_FILE=$3
  KEYID=$4
  IMAGE=ubuntu-20-04-x64
  
  # Code is with TABS NOT -> Spaces
  USER_DATA_CODE=$(cat userdata/$USER_DATA_FILE)
  echo -n "$USER_DATA_CODE" 

  ## 
  # Step 2: Create a new machine and create a random identifier, when can ask for later 
  ##
  echo "--"
  echo "Creating machine with: "
  echo "doctl compute droplet create" 
  echo "--"
  # id of ssh-key is used here 
  DROPLET_ID=$(doctl compute droplet create $MACHINE_ID --size $SIZE --image $IMAGE --region fra1 --ssh-keys "28033186,$KEYID" --wait --no-header --format=ID --user-data "$USER_DATA_CODE")

  echo
  echo "--"
  echo "Assigning droplet to project Training"
  echo "--"
  echo "doctl projects resources assign ${PROJECT_ID} --resource=do:droplet:${DROPLET_ID}"
  doctl projects resources assign ${PROJECT_ID} --resource=do:droplet:${DROPLET_ID}

  # Adding server to known hosts 
  PUBLIC_IP=$(doctl compute droplet get ${DROPLET_ID} --no-header --format PublicIPv4)
  echo $PUBLIC_IP >> .ip_list

}

# This script will create 5 machines - our cluster  

## Step - create new key 
# We need this to deploy the machine (build the cluster) 
# to create a different machine (e.g. for testing), change these two lines
export KEY=~/.ssh/root_digital_ocean_nopw

# generates a new root key for the machine
if [ -f $KEY ]; then
  mv -f $KEY $KEY.backup
fi
ssh-keygen -q -N '' -f $KEY
export TMP_DEPLOY_KEYID=$(doctl compute ssh-key import training_kubernetes \
  --public-key-file $KEY.pub \
  --format ID \
  --no-header)


#machines="client.train.t3isp.de node1.t3isp.de node2.t3isp.de node3.t3isp.de nfs.train.t3isp.de"
#machines="client.train.t3isp.de"
machines="node1a.t3isp.de"

sizes=("s-4vcpu-8gb")
user_data_files=("microk8s.sh")

let idx=0
for m in $machines
do
   echo "--"
   SIZE=${sizes[$idx]}
   USER_DATA_FILE=${user_data_files[$idx]}
   echo $idx "using userdata from file" $USER_DATA_FILE
   # echo "machine: $m -> size "${sizes[$idx]}
   createMachine $m $SIZE $USER_DATA_FILE $TMP_DEPLOY_KEYID
   
   

   let idx=idx+1
done    

###
# Final Step: Cleanup ssh - keys (exported to digitalocean)  
###
echo "Now doing ssh-key cleanups" 
doctl compute ssh-key delete $TMP_DEPLOY_KEYID --force

#echo "Show ssh-keys with command: doctl compute ssh-key list"
#doctl compute ssh-key list
echo "Waiting 10 seconds for the last server to come up (ssh)"
sleep 10
for IP in $(cat .ip_list)
do
  echo $IP
  ssh-keyscan -H $IP >> ~/.ssh/known_hosts 
done	


echo 
echo "--"
echo 

echo "unset ip_list"
echo > .ip_list
