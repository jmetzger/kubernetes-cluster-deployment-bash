#!/bin/bash 

groupadd sshadmin
USERS="11trainingdo"
for USER in $USERS
do
  echo "Adding user $USER"
  useradd -s /bin/bash $USER
  usermod -aG sshadmin $USER
  echo "$USER:11dortmund22" | chpasswd
done

# We can sudo with 11trainingdo
usermod -aG sudo 11trainingdo 

# Setup ssh stuff 
sed -i "s/PasswordAuthentication no/PasswordAuthentication yes/g" /etc/ssh/sshd_config
usermod -aG sshadmin root
echo "AllowGroups sshadmin" >> /etc/ssh/sshd_config 
systemctl reload sshd 

# Now let us do some generic setup 
echo "Installing microk8s"

# This only works with 1.23 out of the box 
# in 1.24 no secrets for service - accounts are created by default 
snap install --classic --channel=1.23/stable microk8s
microk8s enable dns rbac  
echo "alias kubectl='microk8s kubectl'" >> /root/.bashrc
source ~/.bashrc 
alias kubectl='microk8s kubectl'

# Installing nfs-common 
apt-get -y update 
apt-get -y install nfs-common jq

git clone https://github.com/jmetzger/lab-microk8s-pod-security-policies lab 

# set the correct rolebinding for the service-account 
cd /lab 

# Create namespace for testing 
microk8s kubectl create namespace sample-psp

microk8s kubectl apply -f rbac/cluster-role-binding-default-sa-at-kube-system-as-cluster-admin.yml 

# Create a developer role in that namespace 
microk8s kubectl apply -f  rbac/default-sa-at-example-psp-namespace.yaml
# helper - sample psp 
helper/create_sa_kubeconfig.sh default sample-psp

# now we need to modify the setting of kube-api-server
# currently in 1.23 no other admission-plugins are activated 
echo "--enable-admission-plugins=PodSecurityPolicy" >> /var/snap/microk8s/current/args/kube-apiserver
microk8s stop 
microk8s start
