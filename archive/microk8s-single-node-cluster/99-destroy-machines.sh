#!/bin/bash 

## This script will destroy all machines with the project Training 
echo "Show training project and its id: doctl projects list | grep -i training"
doctl projects list | grep -i training
PROJECT_ID=$(doctl projects list | grep -i training | cut -d' ' -f1)
MACHINES=$(doctl projects resources list ${PROJECT_ID} --no-header --format=URN | cut -d ':' -f 3) 

for m in $MACHINES 
do
  echo DELETING $m
  doctl compute droplet delete -f $m 
done


