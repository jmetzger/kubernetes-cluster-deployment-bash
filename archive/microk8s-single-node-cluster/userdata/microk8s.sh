#!/bin/bash 

groupadd sshadmin
USERS="11trainingdo"
for USER in $USERS
do
  echo "Adding user $USER"
  useradd -s /bin/bash $USER
  usermod -aG sshadmin $USER
  echo "$USER:11dortmund22" | chpasswd
done

# We can sudo with 11trainingdo
usermod -aG sudo 11trainingdo 

# Setup ssh stuff 
sed -i "s/PasswordAuthentication no/PasswordAuthentication yes/g" /etc/ssh/sshd_config
usermod -aG sshadmin root
echo "AllowGroups sshadmin" >> /etc/ssh/sshd_config 
systemctl reload sshd 

# Now let us do some generic setup 
echo "Installing microk8s"
snap install --classic microk8s
microk8s enable dns rbac 

# Just in case we need to access it directly 
# on the machine for training purposes
microk8s config > ~/.kube/config 

echo "alias kubectl='microk8s kubectl'" >> /root/.bashrc
source .bashrc 

# Installing nfs-common 
apt-get -y update 
apt-get -y install nfs-common jq


