#!/bin/bash

CURRENT_SCRIPT_DIR=$(pwd)
PROJECT_SCRIPT_DIR=$CURRENT_SCRIPT_DIR"/../kubeadm-3-node-cluster-nonetwork-v2"

echo "1. Extract projects" 


cd multi-data
for PROJECT in *
do 
  CONTROLPLANE_PUBLIC_IP=""


  echo $PROJECT_SCRIPT_DIR/cluster_data/$PROJECT
  if [ -f $PROJECT_SCRIPT_DIR/cluster_data/$PROJECT/controlplane_public_ip ]
  then
    CONTROLPLANE_PUBLIC_IP=$(cat $PROJECT_SCRIPT_DIR/cluster_data/$PROJECT/controlplane_public_ip) 
    echo "Public IP controlplane ($PROJECT):$CONTROLPLANE_PUBLIC_IP"
    echo "Downloading kubeconfig /etc/kubernetes/admin.conf -> to -> $PROJECT/kubeconfig.$PROJECT"
    scp -i ~/.ssh/key_training_kubernetes root@$CONTROLPLANE_PUBLIC_IP:/etc/kubernetes/admin.conf $PROJECT/kubeconfig.$PROJECT 
    chmod 777 $PROJECT/kubeconfig.$PROJECT
 fi 

done

echo "IP :"
read IP 

for PROJECT in *
do 
   if [ -f $PROJECT/kubeconfig.$PROJECT ]
   then 
     echo "Uploading kubeconfig.$PROJECT" 
     scp -i ~/.ssh/key_training_kubernetes $PROJECT/kubeconfig.$PROJECT root@$IP:/tmp
   fi
done

