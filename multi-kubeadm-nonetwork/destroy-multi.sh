#!/bin/bash 

# Currently only this project is support 
PROJECT_SCRIPT_DIR="../kubeadm-3-node-cluster-nonetwork-v2"
PROJECT_SCRIPT="99-delete-project-machines.sh"

for FULL_DIR in multi-data/* 
do 
  MULTI_CLUSTER_PROJECT=$(basename $FULL_DIR)
 
  if [ -d $FULL_DIR ]
  then 
    MULTI_CLUSTER_PROJECT=$(basename $FULL_DIR) 
    cd $PROJECT_SCRIPT_DIR 
    ./$PROJECT_SCRIPT $MULTI_CLUSTER_PROJECT
    echo "Deleting $FULL_DIR"
    cd -
    rm -fR $FULL_DIR
  fi 
done 

