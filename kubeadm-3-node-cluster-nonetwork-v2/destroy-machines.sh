#!/bin/bash 

PROJECT=mcluster-tln1

## This script will destroy all machines with the project Training 
echo "Show $PROJECT project and its id: doctl projects list | grep -i $PROJECT"
doctl projects list | grep -i $PROJECT' '
PROJECT_ID=$(doctl projects list | grep -i $PROJECT' ' | cut -d' ' -f1)
MACHINES=$(doctl projects resources list ${PROJECT_ID} --no-header --format=URN | cut -d ':' -f 3) 

for m in $MACHINES 
do
  echo DELETING $m
  doctl compute droplet delete -f $m 
done


