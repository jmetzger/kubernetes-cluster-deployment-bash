#!/bin/bash 

groupadd sshadmin
USERS="11trainingdo $(echo tln{1..12})"
echo $USERS
for USER in $USERS
do
  echo "Adding user $USER"
  useradd -s /bin/bash $USER
  usermod -aG sshadmin $USER
  echo "$USER:11dortmund22" | chpasswd
done

# We can sudo with 11trainingdo
usermod -aG sudo 11trainingdo 

# Now let us do some generic setup 
echo "Installing kubectl"
snap install --classic kubectl

# Install helm as well 
echo "Installing helm"
snap install --classic helm 

# Setup ssh stuff 
sed -i "s/PasswordAuthentication no/PasswordAuthentication yes/g" /etc/ssh/sshd_config
usermod -aG sshadmin root
echo "AllowGroups sshadmin" >> /etc/ssh/sshd_config 
systemctl reload sshd 

### BASH Completion ###
# update repo 
apt-get update 
apt-get install -y bash-completion
source /usr/share/bash-completion/bash_completion
# is it installed properly
type _init_completion

# activate for all users
kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl > /dev/null

