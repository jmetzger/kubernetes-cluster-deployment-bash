######################################
# Settings specific for project 
######################################

JETZT=$(date)

CLUSTER_NAME=telekom-calico
CLUSTER_PROJECT=cluster-$CLUSTER_NAME

TLN="tln2."
machines="controlplane.${TLN}t3isp.de worker1.${TLN}t3isp.de worker2.${TLN}t3isp.de worker3.${TLN}t3isp.de"
# machines="client.${TLN}t3isp.de controlplane.${TLN}t3isp.de"
sizes=("s-2vcpu-4gb" "s-2vcpu-4gb" "s-2vcpu-4gb" "s-2vcpu-4gb")
user_data_files=("client.sh" "kubernetes-node.sh" "kubernetes-node.sh" "kubernetes-node.sh" "kubernetes-node.sh")

# do we still need that ?  
MACHINES=$machines 


