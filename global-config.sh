# This file only holds configurations, that are generic for all projects 
# WARNING: Put all specific configuuration for the specific project in the folder in config.sh 

# LOGTO=logs/$(date +%Y%m%d%H%M%i)
LOGTO=logs/actions.log

# ssh-key (root) from digitalocean
SSH_ROOT_KEY_ID=38311991

# default settings for connection with ssh 
CMD_SSH="ssh -i ~/.ssh/key_training_kubernetes"
