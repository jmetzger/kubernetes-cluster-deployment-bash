#!/bin/bash 

source config.sh

# Rewrite to array 
let counter=0
for m in $MACHINES 
do 
  machine[counter]=$m
  machine_short[counter]=$(echo $m | cut -d'.' -f1) 
  let counter=counter+1
done

# This should be the control-plane 
CONTROL_PLANE_PUBLIC_IP=$(doctl compute droplet get ${machine[1]} --no-header --format PublicIPv4)
echo $CONTROL_PLANE_PUBLIC_IP 
CNI_CMD="kubectl apply -f https://github.com/antrea-io/antrea/releases/download/v1.13.2/antrea.yml"

# Joining worker1 
ssh -i ~/.ssh/root_digital_ocean_nopw $CONTROL_PLANE_PUBLIC_IP -C "$CNI_CMD"





