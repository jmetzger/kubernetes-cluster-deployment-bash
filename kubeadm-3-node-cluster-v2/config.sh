######################################
# Settings specific for project 
######################################

JETZT=$(date)

CLUSTER_NAME=telekom-calicovxlan
CLUSTER_PROJECT=cluster-$CLUSTER_NAME

TLN="tln2."
machines="client.${TLN}t3isp.de controlplane.${TLN}t3isp.de worker1.${TLN}t3isp.de worker2.${TLN}t3isp.de worker3.${TLN}t3isp.de"
# machines="client.${TLN}t3isp.de controlplane.${TLN}t3isp.de"
sizes=("s-1vcpu-1gb" "s-4vcpu-8gb" "s-4vcpu-8gb" "s-4vcpu-8gb" "s-4vcpu-8gb" "s-1vcpu-1gb")
user_data_files=("client.sh" "kubeadm-controlplane.sh" "kubeadm-worker.sh" "kubeadm-worker.sh" "kubeadm-worker.sh")

# do we still need that ?  
MACHINES=$machines 


