#!/bin/bash 

machines="client.train.t3isp.de node1.t3isp.de node2.t3isp.de node3.t3isp.de"

# Rewrite to array 
let counter=0
for m in $machines 
do 
  machine[counter]=$m 
  let counter=counter+1
done

# first cluster node is machine[1] - node1.t3isp.de 
echo ${machine[1]}

# Let us get public and private ip of first node 
PUBLIC_IP_NODE1=$(doctl compute droplet get ${machine[1]} --no-header --format PublicIPv4)
PUBLIC_IP_CLIENT=$(doctl compute droplet get ${machine[0]} --no-header --format PublicIPv4)

ssh -i ~/.ssh/root_digital_ocean_nopw $PUBLIC_IP_NODE1 -C microk8s config > .microk8s-config
ssh -i ~/.ssh/root_digital_ocean_nopw $PUBLIC_IP_CLIENT -C mkdir -p /root/.kube
scp -i ~/.ssh/root_digital_ocean_nopw .microk8s-config $PUBLIC_IP_CLIENT:/root/.kube/config

# Create the namespaces 
NAMESPACES="$(echo namespace{1..12})"
NAMESPACES2="$(echo namespaces{1..12})"

USERS="$(echo tln{1..12})"

for NS in $NAMESPACES
do
  ssh -i ~/.ssh/root_digital_ocean_nopw $PUBLIC_IP_CLIENT -C kubectl create ns $NS
done 

let COUNTER=1
for USER in $USERS
do
  ssh -i ~/.ssh/root_digital_ocean_nopw $PUBLIC_IP_CLIENT -C "usermod -d /home/$USER -m $USER; mkdir -p /home/$USER/.kube; chown -R $USER:$USER /home/$USER"
  ssh -i ~/.ssh/root_digital_ocean_nopw $PUBLIC_IP_CLIENT -C "kubectl config set-context --current --namespace=namespace$COUNTER; cp /root/.kube/config /home/$USER/.kube/config; chown $USER:$USER /home/$USER/.kube/config" 
  
  let COUNTER=COUNTER+1 
  
done

# reset root/.kube/config
ssh -i ~/.ssh/root_digital_ocean_nopw $PUBLIC_IP_CLIENT -C "kubectl config set-context --current --namespace=default"

