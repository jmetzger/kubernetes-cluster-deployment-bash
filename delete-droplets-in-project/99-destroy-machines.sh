#!/bin/bash 

PROJECT_NAME="karlstorz"

## This script will destroy all machines with the project Training 
echo "Show training project and its id"
doctl projects list --no-header --format ID,Name | grep $PROJECT_NAME
PROJECT_ID=$(doctl projects list --no-header --format ID,Name | grep $PROJECT_NAME | cut -d' ' -f1)
echo $PROJECT_ID
MACHINES=$(doctl projects resources list ${PROJECT_ID} --no-header --format=URN | cut -d ':' -f 3) 

for m in $MACHINES 
do
  echo DELETING $m
  doctl compute droplet delete -f $m 
done


