#!/bin/bash 

# ssh-key (root) from digitalocean
SSH_ROOT_KEY_ID=38311991

. ../global-config.sh
. config.sh

. ../_libs/logging.sh
. ../_libs/machines.sh
. ../_libs/projects.sh 
. ../_libs/ssh_keys.sh

######################################
# Step 1: Get PROJECT_ID 
#         of CLUSTER_PROJECT 
######################################

# Get CLUSTER_PROJECT from commandline 
if [ "$1" != "" ]
then
  CLUSTER_PROJECT=$1
fi   

PROJECT_ID=$(getProjectId $CLUSTER_PROJECT)

if [ "$PROJECT_ID" == "" ]
then
  MSG="Project $CLUSTER_PROJECT not present. Please create manually"
  log $MSG
  echo $MSG
fi

######################################
# Step 2: List droplets from project 
#         & delete them 
######################################

DROPLET_URNS=$(doctl projects resources list $PROJECT_ID --format URN | grep -E "^do")
for DROPLET_URN in $DROPLET_URNS
do

  MSG="Deleting DROPLET_URN $DROPLET_URN"
  echo $MSG
  log $MSG

  # Extract droplet_id
  DROPLET_ID=$(echo $DROPLET_URN | cut -d':' -f3)
  # --force = do not ask
  CMD="doctl compute droplet delete --force $DROPLET_ID"
  echo $CMD
  $CMD
  log $CMD

done

#######################################
# Step 4: Delete project:
#         $CLUSTER_PROJECT 
#######################################

MSG="Deleting Project $PROJECT_ID ($CLUSTER_PROJECT)"
# --force - do not ask 
CMD="doctl projects delete --force $PROJECT_ID"

echo $MSG; log $MSG
$CMD; echo $CMD; log $CMD

MSG="Deleting cache-folder cluster_data/$CLUSTER_PROJECT"
CMD="rm -fR cluster_data/$CLUSTER_PROJECT"

echo $MSG; log $MSG 
$CMD; echo $CMD; log $CMD
  
