#!/bin/bash 

# ssh-key (root) from digitalocean
SSH_ROOT_KEY_ID=38311991

. ../global-config.sh

# Reading config-file from first parameter of call
# Used by multi-cluster/
CONFIG_BY_PARAM=$1
if [ -f $CONFIG_BY_PARAM ]
then
  echo "Loading config $CONFIG_BY_PARAM"
  . $CONFIG_BY_PARAM
else
  echo "Falling back to default config.sh"
  . config.sh
fi

. ../_libs/logging.sh
. ../_libs/machines.sh
. ../_libs/projects.sh 
. ../_libs/ssh_keys.sh

######################################
# Step 1: Get PROJECT_ID 
#         of CLUSTER_PROJECT 
######################################

PROJECT_ID=$(getProjectId $CLUSTER_PROJECT)

if [ "$PROJECT_ID" == "" ]
then
  MSG="Project $CLUSTER_PROJECT not present -> creating it"
  CMD="doctl projects create --no-header --format ID --name $CLUSTER_PROJECT \
       --purpose 'Class project / Educational purposes'"
  
  echo $MSG; log $MSG 
  PROJECT_ID=$($CMD)
  
  MSG="PROJECT_ID: $PROJECT_ID for $CLUSTER_PROJECT created"
  echo $MSG; log $MSG
fi

#######################################
# Step 1.5: Get/Create ssh-key 
#   o Same Key used for all projects 
#######################################

DO_KEY_ID=$(getSshKeyIdFromDo "key_training_kubernetes")
echo $DO_KEY_ID

IMAGE=ubuntu-22-04-x64

######################################
# Step 2: Create the machines 
######################################

MACHINES_PID_LIST=""
let idx=0
for m in $machines
do
   SIZE=${sizes[$idx]}
   USER_DATA_FILE=${user_data_files[$idx]}
   log $idx "using userdata from file $USER_DATA_FILE"
   log "Creating machine $m in the background, yeah !" 
   createMachine $m $SIZE $USER_DATA_FILE "$SSH_ROOT_KEY_ID,$DO_KEY_ID" $IMAGE &
   BG_PID=$!
   MACHINES_PID_LIST=$MACHINES_PID_LIST" "$BG_PID
   let idx=idx+1
done

########################################
# Step 3: Wait till machines are ready
#         by scanning MACHINES_PID_LIST
########################################

for MACHINE_PID in $MACHINES_PID_LIST
do
   PS_REGEX=$PS_REGEX"|"$MACHINE_PID
done

PS_REGEX="("${PS_REGEX:1}")"

while [ $(ps -o pid | grep -c -E $PS_REGEX) -gt 0 ]
do
   log "Still NOT all machines are up. Waiting 5 secs"
   sleep 5 
done 

log "ALL machines are up" 

########################################
# Step 4: Extract droplet_ids 
#         && Write them to cache
########################################

DROPLETS_DIR=cluster_data/$CLUSTER_PROJECT/droplets

for DROPLETS_PATH in $DROPLETS_DIR/*
do 
   # Assemble DROPLET_IDS 
   DROPLETS_HOSTNAME=$(basename $DROPLETS_PATH)
   DROPLETS_HOSTNAME_SHORT=$(echo $DROPLETS_HOSTNAME | cut -d'.' -f 1)
   DROPLETS_ID=$(cat $DROPLETS_PATH/droplet_id)
   DROPLET_IDS_LIST=$DROPLET_IDS_LIST" "$DROPLETS_ID

   MSG="Get public_ip/private_ip for $DROPLETS_HOSTNAME"
   echo $MSG; log $MSG

   #######################
   # Obtain PUBLIC_IP 
   # && Write it to cache 
   #######################

   CMD="doctl compute droplet get $DROPLETS_ID --no-header --format PublicIPv4"
   PUBLIC_IP=$($CMD); echo $CMD; log $CMD 
   echo $PUBLIC_IP > $DROPLETS_PATH/public_ip

   #######################
   # Obtain PRIVATE_IP 
   # && Write it to cache
   #######################

   CMD="doctl compute droplet get $DROPLETS_ID --no-header --format PrivateIPv4"
   PRIVATE_IP=$($CMD); echo $CMD; log $CMD
   echo $PRIVATE_IP > $DROPLETS_PATH/private_ip

   #######################
   # Log both IPS
   #######################
   MSG="PUBLIC_IP: $PUBLIC_IP"; echo $MSG; log $MSG 
   MSG="PRIVATE_IP: $PRIVATE_IP"; echo $MSG; log $MSG

   #######################
   # Assemble IP-Lists
   # 3. WORKER_PUBLIC_IPS 
   #######################
 
   DROPLET_PUBLIC_IPS=$DROPLET_PUBLIC_IPS" "$PUBLIC_IP
   DROPLET_PRIVATE_IPS=$DROPLET_PRIVATE_IPS" "$PRIVATE_IP

   if [ "${DROPLETS_HOSTNAME_SHORT:0:4}" == "node" ]
   then 
     WORKER_PUBLIC_IPS=$WORKER_PUBLIC_IPS" "$PUBLIC_IP
   fi 

   #######################
   # Detect controlplane 
   #######################
   if [ "$DROPLETS_HOSTNAME_SHORT" == "node1" ]
   then 
     echo $PUBLIC_IP > cluster_data/$CLUSTER_PROJECT/controlplane_public_ip     
     CONTROLPLANE_PUBLIC_IP=$PUBLIC_IP
     CONTROLPLANE_PRIVATE_IP=$PRIVATE_IP
   fi 

   ##########################
   # Assemble CMD_HOSTS_FILE
   # o for later usage 
   ##########################

   CMD_HOSTS_FILE=$CMD_HOSTS_FILE"echo $PRIVATE_IP $DROPLETS_HOSTNAME $DROPLETS_HOSTNAME_SHORT >> /etc/hosts;"

done 

########################
# Write to cache 
# 1. DROPLET_IDS_LIST
# 2. DROPLET_PUBLIC_IPS 
########################

echo $DROPLET_IDS_LIST    > cluster_data/$CLUSTER_PROJECT/droplet_ids 
echo $DROPLET_PUBLIC_IPS  > cluster_data/$CLUSTER_PROJECT/public_ips
echo $WORKER_PUBLIC_IPS   > cluster_data/$CLUSTER_PROJECT/worker_public_ips
echo $DROPLET_PRIVATE_IPS > cluster_data/$CLUSTER_PROJECT/private_ips

########################################### 
# Step 5: CareTaker - Operations  
#
# 1. Get PublicIPs 
# 2. ssh-keyscan hosts
# 3. Assign droplets to project "training"
###########################################

# Hack, waiting 10 secs for all hosts to become ready 
# We currently have problems getting the host keys 
sleep 30 
echo "Waiting 30 secs before ssh hosts" && log "waiting 30 secs for we ssh the hosts"

for DROPLET_ID in $DROPLET_IDS_LIST 
do 

   log "Retrieving PUBLIC_IP for DROPLET_ID:$DROPLET_ID"
   PUBLIC_IP=$(doctl compute droplet get ${DROPLET_ID} --no-header --format PublicIPv4)
   
   log "Scanning $PUBLIC_IP (ssh-keyscan)"
   
   # First run 
   ssh-keyscan -H $PUBLIC_IP >> ~/.ssh/known_hosts   
   LAST_RUN=$?

   log "Assign $DROPLET_ID to project $CLUSTER_NAME"
   CMD="doctl projects resources assign "${PROJECT_ID}" --resource=do:droplet:"${DROPLET_ID} 
   log $CMD 
   $CMD  

done 

###############################################
# Step 6: Adjusting hosts file on all machines
###############################################

for PUBLIC_IP in $DROPLET_PUBLIC_IPS 
do 
    MSG="Adjusting /etc/hosts on $PUBLIC_IP" 
    echo $MSG; log $MSG
    ssh -i ~/.ssh/key_training_kubernetes $PUBLIC_IP -C "$CMD_HOSTS_FILE"     
done 

