######################################
# Settings specific for project 
######################################

JETZT=$(date)

CLUSTER_NAME=microk8s-training
CLUSTER_PROJECT=cluster-$CLUSTER_NAME

TLN="tln2."
machines="node1.${TLN}t3isp.de node2.${TLN}t3isp.de node3.${TLN}t3isp.de"
sizes=("s-4vcpu-8gb" "s-4vcpu-8gb" "s-4vcpu-8gb")
user_data_files=("microk8s.sh" "microk8s.sh" "microk8s.sh")

# do we still need that ?  
MACHINES=$machines 


