#!/bin/bash 

# ssh-key (root) from digitalocean
SSH_ROOT_KEY_ID=38311991

###############
# 1 param
###############

if [ $# -eq 1 ]
then
  START=1
  LAST=$1
  HOWMANY=$LAST
fi

###############
# 2 params
###############

if [ $# -eq 2 ]
then
  START=$1
  LAST=$2
  let HOWMANY=$LAST-$START+1
fi

if [ $# -gt 0 ]
then
  echo "Building $HOWMANY Clusters: $START .. $LAST "
else
  echo "Sorry. I have to know howmany clusters"
  echo "e.g. ./create-multi.sh 2"
  echo "Or from START to LAST"
  echo "e.g. ./create-multi.sh 14 15"
  exit
fi

. config.sh
. ../_libs/logging.sh
. ../_libs/projects.sh
. ../_libs/ssh_keys.sh

###########################################
# Step 1: createDoksCluster
###########################################
echo $SIZE

for TLN_NR in $(seq $START $LAST)
do
   # createDoksCluster $SIZE 
   # --wait false... do not wait till completed 
   CURRENT_CLUSTER_NAME=training-tln$TLN_NR
   doctl kubernetes cluster create $CURRENT_CLUSTER_NAME --region fra1 --version 1.31.1-do.2 --node-pool "name=node;size=$SIZE" --wait=false --set-current-context=false 
   mkdir -p multi-data/$CURRENT_CLUSTER_NAME
   CURRENT_CLUSTER_NAME=""
done 

###########################################
# Step 2: Now Set Initial Status 
###########################################

for TLN_NR in $(seq $START $HOWMANY)
do 

  CURRENT_CLUSTER_NAME=training-tln$TLN_NR
  CURRENT_CLUSTER_STATUS=$(doctl kubernetes cluster get $CURRENT_CLUSTER_NAME --no-header --format "Status")
  
  echo "[$(date)] State of cluster $CURRENT_CLUSTER_NAME -> $CURRENT_CLUSTER_STATUS"
  echo $CURRENT_CLUSTER_STATUS > multi-data/$CURRENT_CLUSTER_NAME/status
done

###########################################
# Step 2.5: No clusters ready yet 
###########################################

let COUNT_READY=0



###########################################
# Step 3: Adjusting COUNT_READY if we rerun
#         the script, after having stopped it
############################################

# iterate through all clusters
for TLN_NR in $(seq 1 $HOWMANY)
do
   
   CURRENT_CLUSTER_NAME=training-tln$TLN_NR
   # Add to counter if it is running
   if [ $(cat multi-data/$CURRENT_CLUSTER_NAME/status) == "running" ]
   then
     let COUNT_READY=COUNT_READY+1
   fi
done

echo "$COUNT_READY of $HOWMANY clusters now running (provisioned)"

############################################
# Step 4: Now continously check state 
############################################

while [ $COUNT_READY -lt $HOWMANY ]
do  
   echo "[$(date)]] Waiting 60 seconds till next iteration -> check"
   sleep 60

   # iterate through all clusters 
   for TLN_NR in $(seq 1 $HOWMANY)
   do 
      CURRENT_CLUSTER_NAME=training-tln$TLN_NR 
      # Skip if it is already running
      cat multi-data/$CURRENT_CLUSTER_NAME/status
      if [ $(cat multi-data/$CURRENT_CLUSTER_NAME/status) == "running" ]  
      then
         continue
      fi 

      CURRENT_CLUSTER_STATUS=$(doctl kubernetes cluster get $CURRENT_CLUSTER_NAME --no-header --format "Status")
      echo "[$(date)] State of cluster $CURRENT_CLUSTER_NAME -> $CURRENT_CLUSTER_STATUS"

      if [ "$CURRENT_CLUSTER_STATUS" == "running" ]
      then
          echo $CURRENT_CLUSTER_STATUS > multi-data/$CURRENT_CLUSTER_NAME/status 
          let COUNT_READY=COUNT_READY+1
          echo "$COUNT_READY of $HOWMANY now running (provisioned)"
      fi 

   done 

done
