#!/bin/bash 

# This script will write all cluster kubeconfigs in multi-data 
# cluster-name do have directories 

if [ ! -d kubeconfig-data ]
then 
  mkdir -p kubeconfig-data 
fi 

CURRENT_DIR=$(pwd)
cd multi-data 

for CURRENT_CLUSTER_NAME in $(echo training-*)
do 
  if [ "$CURRENT_CLUSTER_NAME" == 'training-*' ] 
  then 
     echo "no clusters present. Giving Up."
     exit 0 
  fi  

  # Let us keep it simple for now 
  doctl kubernetes cluster kubeconfig show $CURRENT_CLUSTER_NAME > $CURRENT_CLUSTER_NAME/config.$CURRENT_CLUSTER_NAME 
  cp -a $CURRENT_CLUSTER_NAME/config.$CURRENT_CLUSTER_NAME ../kubeconfig-data 
  chmod 777 ../kubeconfig-data/config.$CURRENT_CLUSTER_NAME 
done 

## now tar everything togeter 
cd $CURRENT_DIR 
tar cvfz kubeconfig.tar.gz kubeconfig-data

## 
echo "all kubeconfigs are ready now"
ls -la kubeconfig.tar.gz 

echo "now sending them to client (jump-host)"
echo "ENTER IP"
read IP 
echo "ENTER PASSWORD" 
read PASS

scp -p $PASS kubeconfig.tar.gz 11trainingdo@$IP:/tmp
echo "Done ... THX." 

