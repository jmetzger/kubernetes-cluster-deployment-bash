#!/bin/bash 

# This script will destroy all cluster in multi-data 
# cluster-name do have directories 

CURRENT_DIR=$(pwd)
cd multi-data 

for CURRENT_CLUSTER_NAME in $(echo training-*)
do 
  if [ "$CURRENT_CLUSTER_NAME" == 'training-*' ] 
  then 
     echo "no clusters present. Giving Up."
     exit 0 
  fi  

  echo DELETING $CURRENT_CLUSTER_NAME 
  doctl kubernetes cluster delete --force --dangerous $CURRENT_CLUSTER_NAME 
  # also delete folder 
  rm -fR $CURRENT_CLUSTER_NAME

done 


