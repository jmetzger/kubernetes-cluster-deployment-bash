#!/bin/bash 

HOWMANY=0

if [ "$1" != "" ]
then
  HOWMANY=$1 
fi

# Currently only this project is support 
CURRENT_SCRIPT_DIR=$(pwd)
PROJECT_SCRIPT_DIR=$CURRENT_SCRIPT_DIR"/../microk8s-3-node-cluster-v2"
PROJECT_SCRIPT="01-create-machines.sh"

# What size of droplet do we want
# This will dynamically generate the config
# DROPLET_SIZE=s-4vcpu-8gb
DROPLET_SIZE=s-2vcpu-4gb

cd $PROJECT_SCRIPT_DIR 

if [ $HOWMANY -gt 0 ]
then 
  echo "Building $HOWMANY Clusters"
else 
  echo "Sorry. You have to pass howmany > 0 to the call"
  echo "e.g. ./create-multi.sh 2"
  exit
fi 

for i in $(seq 1 $HOWMANY)
do 
   # echo creating config - folder 
   MULTI_TLN="tln$i"
   MULTI_CLUSTER_NAME=$MULTI_TLN
   MULTI_CLUSTER_PROJECT=cluster-microk8s-$MULTI_TLN
   mkdir -p $CURRENT_SCRIPT_DIR"/multi-data/"$MULTI_CLUSTER_PROJECT
   MULTI_CONFIG_PATH=$CURRENT_SCRIPT_DIR"/multi-data/"$MULTI_CLUSTER_PROJECT"/config.sh"
   MULTI_DOMAIN="do.t3isp.de"

   # write the config 

   echo 'JETZT=$(date)' > $MULTI_CONFIG_PATH
   echo 'CLUSTER_NAME='$MULTI_CLUSTER_NAME >> $MULTI_CONFIG_PATH
   echo 'CLUSTER_PROJECT='$MULTI_CLUSTER_PROJECT >> $MULTI_CONFIG_PATH
   echo 'TLN="'$MULTI_TLN'."' >> $MULTI_CONFIG_PATH
   echo 'DOMAIN="'$MULTI_DOMAIN'"' >> $MULTI_CONFIG_PATH
   echo 'DROPLET_SIZE="'$DROPLET_SIZE'"' >> $MULTI_CONFIG_PATH 
   echo 'machines="node1.${TLN}${DOMAIN} node2.${TLN}${DOMAIN} node3.${TLN}${DOMAIN}"' >> $MULTI_CONFIG_PATH
   echo 'sizes=("${DROPLET_SIZE}" "${DROPLET_SIZE}" "${DROPLET_SIZE}")' >> $MULTI_CONFIG_PATH
   echo 'user_data_files=("microk8s.sh" "microk8s.sh" "microk8s.sh")' >> $MULTI_CONFIG_PATH
   # do we still need that ?
   # echo 'MACHINES=$machines' >> $MULTI_CONFIG_PATH   

   ###########################################
   ## Step 2: Now calling all the subjobs 
   ##         each subjob creates one cluster 
   ###########################################

   cd $PROJECT_SCRIPT_DIR
   # Call Cluster-Creation script 
   # & run it in the background 
   ./$PROJECT_SCRIPT $MULTI_CONFIG_PATH &

   BG_PID=$!
   CLUSTERS_PID_LIST=$CLUSTERS_PID_LIST" "$BG_PID

done

###########################################
# Step 3: Wait till all clusters are ready
#         by scanning MACHINES_PID_LIST
###########################################

for CLUSTER_PID in $CLUSTERS_PID_LIST
do
   PS_REGEX=$PS_REGEX"|"CLUSTER_PID
done

# Get rid of first char 
PS_REGEX="("${PS_REGEX:1}")"

while [ $(ps -o pid | grep -c -E $PS_REGEX) -gt 0 ]
do
   echo "Still NOT all clusters are up. Waiting 5 secs"
   sleep 5
done

echo "ALL clusters are up"
