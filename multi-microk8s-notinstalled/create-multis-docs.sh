#!/bin/bash 

# Currently only this project is support 
CURRENT_SCRIPT_DIR=$(pwd)
PROJECT_SCRIPT_DIR="../microk8s-3-node-cluster-notinstalled-v2"
PROJECT_SCRIPT="99-delete-project-machines.sh"

for FULL_DIR in multi-data/* 
do 
  MULTI_CLUSTER_PROJECT=$(basename $FULL_DIR)
 
  if [ -d $FULL_DIR ]
  then 
    MULTI_CLUSTER_PROJECT=$(basename $FULL_DIR) 
    cd $PROJECT_SCRIPT_DIR 
   
    echo "----------------------" 
    echo $MULTI_CLUSTER_PROJECT      
    echo "----------------------"

    cd cluster_data/$MULTI_CLUSTER_PROJECT 
    
    echo 
    echo "CONTROLPLANE_PUBLIC_IP"
    # echo 
    echo $(cat controlplane_public_ip) 
    echo    
 
    echo "WORKER_PUBLIC_IPS"
    for i in $(cat worker_public_ips)
    do 
       echo $i
    done 

    echo
    echo "----------------------"
    echo "----------------------"
    echo 
    echo
    echo

    # Back to CURRENT_SCRIPT_DIR 
    cd $CURRENT_SCRIPT_DIR      
  fi 
done 

