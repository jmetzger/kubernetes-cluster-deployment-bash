#!/bin/bash 

###############
# 1 param
###############

if [ $# -eq 1 ]
then
  START=1
  LAST=$1
  HOWMANY=$LAST
fi

###############
# 2 params
###############

if [ $# -eq 2 ]
then
  START=$1
  LAST=$2
  let HOWMANY=$LAST-$START+1
fi

if [ $# -gt 0 ]
then
  echo "Building $HOWMANY Clusters: $START .. $LAST "
else
  echo "Sorry. I have to know howmany clusters"
  echo "e.g. ./create-multi.sh 2"
  echo "Or from START to LAST"
  echo "e.g. ./create-multi.sh 14 15"
  exit
fi

if [ "$1" != "" ]
then
  HOWMANY=$1 
fi

# Currently only this project is support 
CURRENT_SCRIPT_DIR=$(pwd)
PROJECT_SCRIPT_DIR=$CURRENT_SCRIPT_DIR"/../kubeadm-3-node-cluster-v2"
PROJECT_SCRIPT="01-create-machines.sh"

cd $PROJECT_SCRIPT_DIR 

for i in $(seq $START $LAST)
do 
   # echo creating config - folder 
   MULTI_TLN="tln$i"
   MULTI_CLUSTER_NAME=$MULTI_TLN
   MULTI_CLUSTER_PROJECT=mcluster-$MULTI_TLN
   mkdir -p $CURRENT_SCRIPT_DIR"/multi-data/"$MULTI_CLUSTER_PROJECT
   MULTI_CONFIG_PATH=$CURRENT_SCRIPT_DIR"/multi-data/"$MULTI_CLUSTER_PROJECT"/config.sh"
   MULTI_DOMAIN="do.t3isp.de"

   # write the config 

   echo 'JETZT=$(date)' > $MULTI_CONFIG_PATH
   echo 'CLUSTER_NAME='$MULTI_CLUSTER_NAME >> $MULTI_CONFIG_PATH
   echo 'CLUSTER_PROJECT='$MULTI_CLUSTER_PROJECT >> $MULTI_CONFIG_PATH
   echo 'TLN="'$MULTI_TLN'."' >> $MULTI_CONFIG_PATH
   echo 'DOMAIN="'$MULTI_DOMAIN'"' >> $MULTI_CONFIG_PATH
   echo 'machines="controlplane.${TLN}${DOMAIN} worker1.${TLN}${DOMAIN} worker2.${TLN}${DOMAIN} worker3.${TLN}${DOMAIN}"' >> $MULTI_CONFIG_PATH
   echo 'sizes=("s-2vcpu-4gb" "s-2vcpu-4gb" "s-2vcpu-4gb" "s-2vcpu-4gb")' >> $MULTI_CONFIG_PATH
   echo 'user_data_files=("kubeadm-controlplane.sh" "kubeadm-worker.sh" "kubeadm-worker.sh" "kubeadm-worker.sh")' >> $MULTI_CONFIG_PATH

   ###########################################
   ## Step 2: Now calling all the subjobs 
   ##         each subjob creates one cluster 
   ###########################################

   cd $PROJECT_SCRIPT_DIR
   # Call Cluster-Creation script 
   # & run it in the background 
   ./$PROJECT_SCRIPT $MULTI_CONFIG_PATH &

   BG_PID=$!
   CLUSTERS_PID_LIST=$CLUSTERS_PID_LIST" "$BG_PID

done

###########################################
# Step 3: Wait till all clusters are ready
#         by scanning MACHINES_PID_LIST
###########################################

for CLUSTER_PID in $CLUSTERS_PID_LIST
do
   PS_REGEX=$PS_REGEX"|"CLUSTER_PID
done

# Get rid of first char 
PS_REGEX="("${PS_REGEX:1}")"

while [ $(ps -o pid | grep -c -E $PS_REGEX) -gt 0 ]
do
   echo "Still NOT all clusters are up. Waiting 5 secs"
   sleep 5
done

echo "ALL clusters are up"
