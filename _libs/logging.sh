#!/bin/bash 

##################################################
## Logging - Functions
##################################################

##################################################
# Function: log 
# $1 message
##################################################

mkdir -p logs  

function log {

  # Format: YYYY-MM-DD HH:MM:SS  
  echo [$(date +'%Y-%m-%d %H:%M:%S')] $1 >> $LOGTO  

}

##################################################
# Function: elog 
# - echo and log in one command
##################################################

function elog {
   
   echo $1 
   log $1   

}
