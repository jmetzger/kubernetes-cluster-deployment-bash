#!/bin/bash 


##################################################
## Functions
##################################################

##################################################
# Function: getProjectId 
# $1 PROJECT_NAME
##################################################

function getProjectId {

  PROJECT_NAME=$1
  log "Show training project and its id: doctl projects list | grep -i $1"
  log $(doctl projects list | grep -i "$1")
  # Return PROJECT_ID from digitalocean account
  echo $(doctl projects list | grep -i -E ' '$PROJECT_NAME' ' | cut -d' ' -f1)

}
