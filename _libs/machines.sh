#!/bin/bash 


##################################################
## Functions
##################################################

##################################################
# Function: createMachine 
# $1 MACHINE_ID
# $2 SIZE
# $3 USER_DATA_FILE 
# $4 IMAGE 
##################################################

function createMachine {

  MACHINE_ID=$1
  SIZE=$2
  USER_DATA_FILE=$3
  KEYID=$4
  IMAGE=$5
  # IMAGE=ubuntu-20-04-x64
  
  # Code is with TABS NOT -> Spaces
  USER_DATA_CODE=$(cat userdata/$USER_DATA_FILE)
  log "$USER_DATA_CODE" 

  log "--"
  log "Creating machine with: "
  log "doctl compute droplet create" 


  log "command::doctl compute "'droplet create '$MACHINE_ID' --size '$SIZE' --image '$IMAGE' --region fra1 
--ssh-keys '"$KEYID"' --wait --no-header --format=ID --user-data '"$USER_DATA_CODE"

  log "--"
  # id of ssh-key is used here 
  DROPLET_ID=$(doctl compute droplet create $MACHINE_ID --size $SIZE --image $IMAGE --region fra1 --ssh-keys "$KEYID" --wait --no-header --format=ID --user-data "$USER_DATA_CODE")

  mkdir -p cluster_data/$CLUSTER_PROJECT/droplets/$MACHINE_ID 
  echo $DROPLET_ID > cluster_data/$CLUSTER_PROJECT/droplets/$MACHINE_ID/droplet_id 

}

