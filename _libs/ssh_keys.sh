#!/bin/bash 


##################################################
## Functions
##################################################

##################################################
# Function: isSshkeyByName 
# $1 SSH_KEY_NAME in Account 
##################################################

function isSshkeyByNameInAccount {

  NAME=$1
  log "Is Key $NAME present in account ?"
  COUNT=$(doctl compute ssh-key list --format "Name" | grep -c "$NAME")

  if [ $COUNT -gt 0 ]
  then 
    log "Key $NAME is present in Account"
    return 0
  else 
    log "Key $NAME is NOT yet present in Account"
    return 1 
  fi

}

#################################################
# Function: getSshKeyIdFromDo
# $1 SSH_KEY_NAME in Account 
#################################################

function getSshKeyIdFromDo {

   KEY=$1
   KEY_FILE=~/.ssh/$KEY

   ###########################################
   # Step 1: Create Key if not present yet 
   #         in docker container 
   ###########################################

   if [ ! -f $KEY_FILE ]
   then
     log "Generating ssh-key - pair for $KEY" 
     ssh-keygen -q -N '' -f $KEY_FILE
     log "Return Code:"$?
     if [ $? -ne 0 ]
     then
       log "Something went wrong will created pub/private key-pair $KEY"
       return 1
     fi    
   fi

   ###########################################
   # Step 2: Import key to account if not 
   #         present 
   ###########################################

   isSshkeyByNameInAccount $KEY

   # Not present yet, so import  
   if [ $? -ne 0 ]
   then 
     log "Importing ssh-key $KEY into account"
     KEYID=$(doctl compute ssh-key import $KEY \
               --public-key-file $KEY_FILE.pub \
               --format ID \
               --no-header)     

     if [ $? -eq 0 ]
     then
       echo $KEYID 
       return 0
     fi 

     ## Catching a possible error ##  
     log "Something went wrong while importing $KEY to digitalocean"
     echo 0
     return 1
     
   fi
    
   # Present, so we just grab it 
   log "Retrieving DO_KEY_ID from account now" 
   echo $(getSshKeyIdByNameFromDo $KEY) 
   return 0

} 


function getSshKeyIdByNameFromDo {

   NAME=$1
   echo $(doctl compute ssh-key list --no-header | grep "$NAME" | cut -d " " -f1)

}
