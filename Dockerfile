FROM digitalocean/doctl 
COPY . .
RUN ln -s /app/doctl /usr/local/bin/doctl && \
apk add git vim bash-completion && \
git config --global user.email "j.metzger@t3company.de" && \
git config --global user.name "Jochen Metzger" && \
doctl completion bash > /usr/share/bash-completion/completions/doctl  
# This loads the bash-completion correctly 
ENTRYPOINT ["/bin/bash","-l"]
